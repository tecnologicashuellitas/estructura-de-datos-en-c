
#include<stdio.h> 
#include<stdlib.h> //Biblioteca est�ndar para conversiones de datos.

//CREACI�N DE ESTRUCTURA.
/*
	Estructura "tipoNodo" que contiene dos elementos:
	 
	informacion: tipo entero.
	*liga: apuntador a estructuras de tipo nodo. 
*/ 
typedef struct nodo 
{ 
    int informacion; 
    struct nodo *liga; 
}tipoNodo; 

typedef tipoNodo *pLista; //Se crea un tipo de dato que es un apuntador llamado *Lista que apunta a una tipoNodo.

//PROTOTIPOS DE FUNCIONES. 
void creafinal(pLista *P); 
void recorreinterativo(pLista P); 
pLista encontrar_medio(pLista primero, pLista ultimo); 
pLista busqueda_binaria(pLista P, int valor); 
int lista_llena(); 

//FUNCIONES DE LISTAS PARA UNA B�SQUEDA BINARIA. 
  /*
	Name: creafinal
	Copyright: FAHJ  & DVLA
	Date: 20/10/19 19:42. 
	Description: Funci�n para iniciar y crear nodos en la lista simple
	Par�metros: 
	pLista *P: apuntador pasado por referencia. 
	
*/
void creafinal(pLista *P){
	
		pLista Q = NULL; //Se crean apuntadores de tipo pLista y se inicializan en NULL
		pLista T = NULL;  
		
	int infoP, infoQ; //Variables enteras. 
	int selector; 
	

	if((lista_llena())== 1 ){
		
		printf("\n\nNo hay espacio en la memoria "); 
		return ; 
	}
	
	*P = (pLista)malloc(sizeof(tipoNodo)); //Se reserva espacio en memoria para *p
	
	
	printf("\n\nIngrese un dato entero para  para P:"); 
	scanf("%d", &infoP); 
	(*P) -> informacion = infoP; 
	(*P) -> liga = NULL; 
	T = *P; 
	
	 do{ //Hacer...
	 
	 		if((lista_llena())== 1 ){
			printf("\n\nNo hay espacio en la memoria "); 
			return ; 
			}
	  
	   		Q = (pLista)malloc(sizeof(tipoNodo)); //Se reserva espacio en memoria para q. 
	   		
	 		printf("\nIngrese en elemento para 	Q:"); 
			scanf("%d", &infoQ); 
			
			Q -> informacion = infoQ; 
			Q -> liga = NULL; 
			T -> liga = Q; 
			T = Q;
			 
			printf("\n\nDesea seguir ingresando elementos ? \nIngrese 5: Salir. \nIngrese cualquier otro numero para continuar: "); 
			scanf("%d",&selector); 
	
	
		}while ( selector != 5); //Mientras el selector u opci�n sea diferente de 5
		
}//FIN CREA FINAL

 /*
	Name: recorreiterativo
	Copyright: FAHJ  & DVLA
	Date: 20/10/19 19:42. 
	Description: Funci�n para recorrer la lista
	Par�metros: 
	pLista P: apuntador por valor  
	
*/

void recorreiterativo(pLista P){
	
	pLista Q; //Se crea un apuntador Q de tipo pLista. 
	
	Q = P; 
	
	if(Q == NULL){
		
		printf("\n\nNo hay elementos en la lista"); 
	    return; 
	    
	}
	while ( Q != NULL){
		
		printf("\nLa informacion contenida en la lista es: "); 
		printf("%d ", Q -> informacion); 
		Q = Q -> liga; 
		
	}
	
}//FIN RECORRE ITERATIVO 

 /*
	Name: creafinal
	Copyright: FAHJ  & DVLA 
	Date: 20/10/19 19:42. 
	Description: Funci�n para encontrar se�alar al nodo que contiene el  elemento medio de la lista
	Par�metros: 
	pLista primero: apuntador al principio de la lista
	pLista ultimo: apuntador al final de la lista. 
	
*/ 
pLista encontrar_medio(pLista primero, pLista ultimo) 
{ 
    if (primero == NULL) 
        return NULL; 
  
    pLista aux_medio = primero; 
    pLista auxiliar = primero -> liga; 
  
    while (auxiliar != ultimo) 
    { 
        auxiliar = auxiliar -> liga; 
        if (auxiliar != ultimo) 
        { 
            aux_medio = aux_medio -> liga; 
            auxiliar = auxiliar -> liga; 
        } 
    } 
  
    return aux_medio; 
    
}//FIN CREA FINAL. 

 /*
	Name: busqueda_binaria
	Copyright: FAHJ  & DVLA 
	Date: 20/10/19 19:42. 
	Description: Funci�n para encontrar el elemento ingresado 
	Par�metros: 
	pLista P: apuntador pasado por valor
	int valor: Un n�mero entero para buscarlo en la lista
	 
	
*/
pLista busqueda_binaria(pLista P, int valor) 
{ 
    pLista primero = P; //inferior 
    pLista ultimo = NULL;  //Superior
  
    do
    { 
        
        pLista medio = encontrar_medio(primero, ultimo); //Encontramos el nodo que contiene en nodo->informacion al numero medio 
  
        // S� la lista esta vacia
        if (medio == NULL) 
            return NULL; 
  
        //S� el valor a buscar es igual a medio 
        if (medio -> informacion == valor) 
            return medio;  //Regresamos el valor que estamos buscando 
  
       //Si  medio es mayor que el valor  a buscar   medio>valor  
        else if (medio -> informacion > valor) 
             ultimo = medio;
  
        // Si el valor medio es menor que valor medio<valor
        else
             
        primero = medio -> liga; 
  
    } while (ultimo == NULL || 
             ultimo != primero); 
    // En caso de que el elemento no se haya encontrado 
    return NULL; 
    
}//FIN BUSQUEDA BINARIA

 /*
	Name: lista_llena
	Copyright: FAHJ  & DVLA
	Date: 20/10/19 19:42. 
	Description: Funci�n que indica si la lista esta llena 
	1: La lista se encuentra llena
	0: A�n hay espacio en la memoria 
	
*/

int lista_llena(){
	
		pLista aux;

	if( (aux = (pLista)malloc(sizeof(tipoNodo))) == NULL ){
		
		return 1;
		
	} else
	
		return 0;

} //FIN LISTA LLENA.

/*
  Name: Funci�n principal (main). 
  Copyright: CDMX, 2019.
  Date: 20/10/2019.  
  Description: Funci�n principal que ejecuta el programa.
  Tipo: void 
  Par�metros: 
  			Ninguno.
  Regresa:	enteros (int)
  			0 : cuando no hay errores. 
			1: cuando existe un error en la funci�n principal.  
*/
int main() 
{ 
	pLista inicio = NULL;  
	int valor; 
	int eSelector; 
	
	do{ //Repetir...
		
			printf("\n\nMenu Lista"); 
			printf("\n\n1-Crea final\n2-Ver lista\n3-Busqueda binaria\n0-Salir"); 
			printf("\n\nIngrese una opcion: "); 
			scanf("%d", &eSelector); 
			
			switch(eSelector){ //En caso de eSelector hacer...
				
					case 1: creafinal(&inicio);
							break; 
							
					case 2: recorreiterativo(inicio); 
							break;
							
					case 3: if(inicio == NULL){
						
								printf("\nNo hay elementos en la lista"); 
								
					        }else{
							
								printf("\nIngre un numero a buscar:"); 
								scanf("%d",&valor); 
								if (busqueda_binaria(inicio, valor) == NULL) 
									
									printf("\n\nEl valor no esta en la lista"); 
								
								else
									
									printf("\n\nEl valor esta en la lista");
								
								 
					
							}
							
							break;
							
					default: printf("\nError en la seleccion");
			}
	 
	}while(eSelector !=0); //Mientras la opci�n seleccionada sea diferente de cero...
    
   return 0; //Regresa 0, la funci�n principal se ejecut� sin errores. 
} 
